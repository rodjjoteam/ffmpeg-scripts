Save Hard Disc space by compressing your movies, series, animes.


"High Efficiency Video Coding (HEVC), also known as H.265, is a video compression standard, one of several potential successors to the widely used AVC (H.264 or MPEG-4 Part 10). In comparison to AVC, HEVC offers about double the data compression ratio at the same level of video quality, or substantially improved video quality at the same bit rate. It supports resolutions up to 8192×4320, including 8K UHD." (https://en.wikipedia.org/wiki/High_Efficiency_Video_Coding)


ffmpeg-x265-copy_audio-half-bitrate.py checks the video file then if it does not contains HEVC, converts it using about 1/2 of original video bitrate.

result: video files with about 0.5 to 0.6 size of origianl one.

# windows 32 bits download:
https://ffmpeg.zeranoe.com/builds/win32/static/ffmpeg-20160718-450cf40-win32-static.zip


# window 64 bits download:
https://ffmpeg.zeranoe.com/builds/win64/static/ffmpeg-20160718-450cf40-win64-static.zip

# Get the script here:
https://bitbucket.org/rodjjoteam/ffmpeg-scripts/downloads/ffmpeg-x265-copy_audio-half-bitrate.py

# installation:

* 1 - Install python version 3 or above. https://www.python.org/downloads/release/python-350/

![win_installer.png](https://bitbucket.org/repo/jLnznn/images/557758901-win_installer.png)

* 2 - download ffmpeg (see links above).
* 3 - extract ffmpeg files to the script folder.

![sample03.png](https://bitbucket.org/repo/jLnznn/images/2112989979-sample03.png)

* 4 - open windows explorer in shell:sendto.
* 5 - create shortcut to ffmpeg-x265-copy_audio-half-bitrate.py there. [How to create shortcuts](http://www.computerhope.com/issues/ch000739.htm)

![sample01.png](https://bitbucket.org/repo/jLnznn/images/1093839657-sample01.png)

* 6 - select one or more files and use windows's explorer menu "send to"

![sample04.png](https://bitbucket.org/repo/jLnznn/images/2081814890-sample04.png)

* 7 - Check your converted videos (Videos Directory).

![sample05.png](https://bitbucket.org/repo/jLnznn/images/3866136943-sample05.png)