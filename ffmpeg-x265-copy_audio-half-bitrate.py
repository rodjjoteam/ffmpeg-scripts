import sys
import re
import os
import string
import random
import ctypes
import subprocess
import winsound
import time
from datetime import datetime, timedelta

BIT_RATE_REGEX = re.compile("Video\\: (?!hevc)[^\\n\\r]+,[ ]*([0-9]+)[ ]*kb\\/s", re.IGNORECASE)
H265_REGEX = re.compile("Video\\: hevc[^\\n\\r]+,", re.IGNORECASE)
BIT_RATE_REGEX2 = re.compile("Duration:[^\\n\\r]+bitrate:[ ]+([0-9]+)[ ]*kb\\/s")
AAC_AUDIO_REGEX = re.compile("Stream[^\\n\\r]+Audio\\:[ ]+aac[ ]+", re.IGNORECASE)
CURRENT_TIME_REGEX = re.compile("([0-9]{2}:[0-9]{2}:[0-9]{2})")
DURATION_REGEX = re.compile("Duration:[ ]+([0-9]{2}:[0-9]{2}:[0-9]{2})")

DRIVER_NAME_REGEX = re.compile("([a-z]:[\\/])", re.IGNORECASE)
FILE_NAME_REGEX = re.compile("([^/\\\\]+)\.[a-z0-9]{1,3}$")

ZERO_TIME = datetime.min.time()

eta = ""
total_files = 0
current_file = 0
current_path = ""
current_bitrate = ""
start_time = 0
movie_duration = None
last_elapsed_time = 0
last_conversion_position = 0
conversion_speed = None

def parse_seconds(time_str):
    time_value = datetime.strptime(time_str, "%H:%M:%S")
    return timedelta(hours=time_value.hour, minutes=time_value.minute, seconds=time_value.second).total_seconds()
    

def reset_progress(probe_result):
    global start_time
    global movie_duration
    global eta
    global last_elapsed_time
    global last_conversion_position
    global conversion_speed
    conversion_speed = None
    last_elapsed_time = 0
    last_conversion_position = 0
    eta = "computing progress"
    movie_duration = None
    duration = DURATION_REGEX.search(probe_result)
    if duration:
        movie_duration = parse_seconds(duration.group(1))
    start_time = datetime.now()
    
def get_elapsed_time():
    global start_time
    return (datetime.now() - start_time).total_seconds()
    
def totimestr(seconds):
    return time.strftime('%H:%M:%S', time.gmtime(int(seconds)))
    
def compute_progress(ffmpeg_line):
    if movie_duration is None:
        return
    global eta
    global last_elapsed_time
    global last_conversion_position
    global conversion_speed
    curret_elapsed_time = get_elapsed_time()
    if curret_elapsed_time >= 1:
        parsed_position = CURRENT_TIME_REGEX.search(ffmpeg_line)
        if parsed_position:
            current_conversion_position = parse_seconds(parsed_position.group(1))
            if (current_conversion_position - last_conversion_position) > 0:
                if conversion_speed  is None or (curret_elapsed_time - last_elapsed_time) > 30:
                    conversion_speed = (curret_elapsed_time - last_elapsed_time) / (current_conversion_position - last_conversion_position)
                    last_conversion_position = current_conversion_position
                    last_elapsed_time = curret_elapsed_time
                seconds_to_finish = (movie_duration - current_conversion_position) * conversion_speed
                finish_at = (datetime.now() + timedelta(seconds=seconds_to_finish))
                percent = "{}%".format(int(round((100 /movie_duration) * current_conversion_position, 0)))
                eta = "[H265 codec] {current_file}/{total_files} time({start_time}-{time_spent})->({finish}-{spending}) progress({percent}-{pos}/{duration}) ({speed}x eta: {eta})".format(
                    current_file=current_file, total_files=total_files, start_time=start_time.strftime('%H:%M:%S'), time_spent=totimestr(curret_elapsed_time), 
                    pos=totimestr(current_conversion_position), duration=totimestr(movie_duration), speed=round(1 / conversion_speed if conversion_speed else 0.0, 2), eta=totimestr(seconds_to_finish), 
                    finish=finish_at.strftime('%H:%M:%S'), spending=totimestr(curret_elapsed_time + seconds_to_finish), percent=percent
                )
    ctypes.windll.kernel32.SetConsoleTitleW(eta)

    
def generate_filename(source_path):
    user_dir = os.path.expanduser("~")
    videos_dir = os.path.join(user_dir, "videos")
    if not os.path.exists(videos_dir):
        videos_dir = user_dir
    name = FILE_NAME_REGEX.search(source_path)
    if name is None:
        name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
    else:
        name = name.group(1)
    return os.path.join(videos_dir, name + ".x265.mp4")
    
def get_script_dir():
    return os.path.dirname(os.path.realpath(__file__))

def ffmpeg_path():
    return os.path.join(get_script_dir(), "ffmpeg.exe")

def ffprobe_path():
    return os.path.join(get_script_dir(), "ffprobe.exe")
    
    
def convert_file(source_path, minimal_bitrate, should_convert_audio):
    params = [
        ffmpeg_path(),  
        "-i", source_path, 
        "-c:v", "libx265", 
        "-b:v", str(minimal_bitrate) + "k",  
        "-minrate", str(minimal_bitrate) + "k",  
        "-preset", "ultrafast",
        "-loglevel", "info"
    ]
    if should_convert_audio:
        params += ["-c:a", "aac", "-b:a", "128k"]
    else:
        params += ["-c:a", "copy"]
    video_path = generate_filename(source_path)
    if os.path.exists(video_path):
        os.remove(video_path)
    params += [video_path]
    process = subprocess.Popen(params, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
    for line in process.stdout:
        compute_progress(line)
    winsound.Beep(3000, 1000)
    winsound.Beep(1000, 1000)

def probe_video(file_path):
    p = subprocess.Popen([ffprobe_path(), file_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    return err.decode("utf-8", errors='replace')
    
    
def is_h265(probe_result):
    return H265_REGEX.search(probe_result) is not None
    
def get_bitrate(probe_result):
    bit_rate = BIT_RATE_REGEX.search(probe_result)
    if bit_rate is None:
        bit_rate = BIT_RATE_REGEX2.search(probe_result)
    if bit_rate:
        return int(bit_rate.group(1))
    return None

def is_not_aac_audio_codec(probe_result):
    return AAC_AUDIO_REGEX.search(probe_result) is None
    
def print_usage():
    print("usage: this_script.py <video_path> [video_path2] ...\n")
    sys.exit(0)

def ShortPathName(name, max_size):
    name_len = len(name)
    if max_size <= 0 or name_len < 2:
        return name, max_size
    if name_len > max_size:
        name_len = name_len - max_size
    else:
        name_len = 1
    max_size = max_size - (len(name) - name_len)
    return name[0:name_len] + "*", max_size
    
    
def ShortPathStr(path, max_size=50):
    # c:\test\test\a dir big path\a name of a file.mp4   -> c:/test/t../a.../a name of a file.mp4
    if len(path) <= max_size:
        return path
    size_diff = len(path) - max_size
    driver, path = os.path.splitdrive(path)
    path, filename = os.path.split(path)
    directories = []
    while 1:
        path, folder = os.path.split(path)
        if folder != "":
            directories.append(folder)
        else:
            if path != "":
                directories.append(path)
            break
    size_diff += 3
    for p in range(0, len(directories) - 1):
        directories[p], size_diff = ShortPathName(directories[p], size_diff)
        if size_diff <= 0: 
            break
    if size_diff >= 0:
        filename, _ = ShortPathName(filename, size_diff)
    directories.reverse()
    result = driver[0:2]
    for d in directories:
        result += d + ("\\" if "\\" not in d and "/" not in d else "")
    result += filename
    return result
    
    
def process_paths():
    global total_files 
    global current_file
    global current_path
    global current_bitrate
    files = sys.argv[1:]
    total_files = len(files)
    current_file = 0
    print("converting {} file(s) to h265 codec using 1/2 of original bitrate".format(total_files))
    print("files will be in videos directory at the path: {}\n".format(os.path.expanduser("~")))
    for current_path in files:
        current_file += 1
        probe_result = probe_video(current_path)
        if is_h265(probe_result):
            print("\n\"{}\"  is h265 already\n\n".format(current_path))
            continue
        current_bitrate = get_bitrate(probe_result)
        if current_bitrate is None:
            print("\"{}\"  can not get bitrate".format(current_path))
            continue
        current_bitrate = int(current_bitrate * 0.50)
        info = "{} {}mb/s".format(total_files - (current_file - 1), round(current_bitrate / 1024, 1))
        print("{} \"{}\"".format(info, ShortPathStr(current_path.encode('ascii', 'ignore').decode('utf-8'), 70 - len(info))), end="")
        sys.stdout.flush()
        reset_progress(probe_result)
        convert_file(current_path, current_bitrate, is_not_aac_audio_codec(probe_result))
        print(" done!")
    if total_files > 1:
        winsound.Beep(3000, 1000)
        winsound.Beep(2000, 1000)
        winsound.Beep(3000, 1000)
    
if __name__ == "__main__":
    if not os.path.exists(ffmpeg_path()) or not os.path.exists(ffprobe_path()):
        print("ffmpeg.exe and ffprobe.exe required.")
        sys.stdin.read(1)
        sys.exit(1)
    if len(sys.argv) < 2:
        print_usage()
    process_paths()
    # subprocess.Popen(["cmd", "/c", "shutdown", "-s", "-t", "30"])
    
